# Author : Exceptional_Group
# date: 11/july/2018
# Version: alpha 0.1


from time import sleep
import RPi.GPIO as GPIO
import os
import glob
import time

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'


def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines


def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos + 2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c


GPIO.setmode(GPIO.BCM)


# Used pins of the ULN2003A on the pins of the Raspberry Pi
# assigned
# Physical pins 11,15,16,18
# GPIO17,GPIO22,GPIO23,GPIO24
IN1 = 17  # IN1
IN2 = 22  # IN2
IN3 = 23  # IN3
IN4 = 24  # IN4

# Waiting time controls the speed of how fast the engine is running
# turns.
time1 = 0.001

# Define pins from outputs
GPIO.setup(IN1, GPIO.OUT)
GPIO.setup(IN2, GPIO.OUT)
GPIO.setup(IN3, GPIO.OUT)
GPIO.setup(IN4, GPIO.OUT)

# All pins are initially set to false. That's how it turns
# Stepper motor not immediately anyway.
GPIO.output(IN1, False)
GPIO.output(IN2, False)
GPIO.output(IN3, False)
GPIO.output(IN4, False)


# The stepper motor 28BYJ-48 is constructed in such a way that the motor in the
# Inside 8 steps needed for a turn. By the companies
# but it requires 512 x 8 steps so that the axis once around
# itself turns so 360 turns.


# Definition of steps 1 - 8 via the pins IN1 to IN4
# Between each movement of the engine is briefly waiting for the
# Engine anchor reaches its position.
def Step1():
    GPIO.output(IN4, True)
    sleep(time1)
    GPIO.output(IN4, False)


def Step2():
    GPIO.output(IN4, True)
    GPIO.output(IN3, True)
    sleep(time1)
    GPIO.output(IN4, False)
    GPIO.output(IN3, False)


def Step3():
    GPIO.output(IN3, True)
    sleep(time1)
    GPIO.output(IN3, False)


def Step4():
    GPIO.output(IN2, True)
    GPIO.output(IN3, True)
    sleep(time1)
    GPIO.output(IN2, False)
    GPIO.output(IN3, False)


def Step5():
    GPIO.output(IN2, True)
    sleep(time1)
    GPIO.output(IN2, False)


def Step6():
    GPIO.output(IN1, True)
    GPIO.output(IN2, True)
    sleep(time1)
    GPIO.output(IN1, False)
    GPIO.output(IN2, False)


def Step7():
    GPIO.output(IN1, True)
    sleep(time1)
    GPIO.output(IN1, False)


def Step8():
    GPIO.output(IN4, True)
    GPIO.output(IN1, True)
    sleep(time1)
    GPIO.output(IN4, False)
    GPIO.output(IN1, False)

# Turn left around
def left(step):
    for i in range(step):
        # os.system('clear') # slows down the movement of the engine too much.
        Step1()
        Step2()
        Step3()
        Step4()
        Step5()
        Step6()
        Step7()
        Step8()
        print("Step left: ", i)


# Turn clockwise (right around)
def right(step):
    for i in range(step):
        # os.system('clear') # slows down the movement of the engine too much.
        Step8()
        Step7()
        Step6()
        Step5()
        Step4()
        Step3()
        Step2()
        Step1()
        print("Step right: ", i)


cond = 0
# Here the decision turns left or right
while True:
    tmp = read_temp()
    time.sleep(5)
    if tmp >= 28 and cond != 1:
        cond = 1
        # Here it is determined how far the engine turns.
        left(100)
    elif tmp < 28 and cond != -1:
        cond = -1
        right(100)
    GPIO.cleanup()
